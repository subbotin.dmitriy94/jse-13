package com.tsconsulting.dsubbotin.tm.controller;

import com.tsconsulting.dsubbotin.tm.api.controller.IProjectController;
import com.tsconsulting.dsubbotin.tm.api.service.IProjectService;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProject() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[Create]");
    }

    @Override
    public void showProjects() {
        if (projectService.findAll().isEmpty()) System.out.println("[List projects empty]");
        int index = 1;
        final List<Project> projects = projectService.findAll();
        for (Project project : projects) System.out.println(index++ + ". " + project);
    }

    @Override
    public void clearProjects() {
        projectService.clear();
        System.out.println("[Clear]");
    }

    @Override
    public void showById() {
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("[Project not found]");
            return;
        }
        showProject(project);
    }

    @Override
    public void showByIndex() {
        System.out.println("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("[Project not found]");
            return;
        }
        showProject(project);
    }

    @Override
    public void showByName() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findByName(name);
        if (project == null) {
            System.out.println("[Project not found]");
            return;
        }
        showProject(project);
    }

    @Override
    public void updateById() {
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        if (projectService.findById(id) == null) {
            System.out.println("[Project not found]");
            return;
        }
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        Project updatedProject = projectService.updateById(id, name, description);
        if (updatedProject == null) System.out.println("[Incorrect value]");
        else System.out.println("[Updated project]");
    }

    @Override
    public void updateByIndex() {
        System.out.println("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        if (projectService.findByIndex(index) == null) {
            System.out.println("[Project not found]");
            return;
        }
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        Project updatedProject = projectService.updateByIndex(index, name, description);
        if (updatedProject == null) System.out.println("[Incorrect value]");
        else System.out.println("[Updated project]");
    }

    @Override
    public void startById() {
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.startById(id);
        if (project == null) System.out.println("[Project not found]");
    }

    @Override
    public void startByIndex() {
        System.out.println("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.startByIndex(index);
        if (project == null) System.out.println("[Project not found]");
    }

    @Override
    public void startByName() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.startByName(name);
        if (project == null) System.out.println("[Project not found]");
    }

    @Override
    public void finishById() {
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.finishById(id);
        if (project == null) System.out.println("[Project not found]");
    }

    @Override
    public void finishByIndex() {
        System.out.println("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.finishByIndex(index);
        if (project == null) System.out.println("[Project not found]");
    }

    @Override
    public void finishByName() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.finishByName(name);
        if (project == null) System.out.println("[Project not found]");
    }

    @Override
    public void updateStatusById() {
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        if (projectService.findById(id) == null) {
            System.out.println("[Project not found]");
            return;
        }
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        Status status = Status.valueOf(statusValue);
        final Project updatedStatusProject = projectService.updateStatusById(id, status);
        if (updatedStatusProject == null) System.out.println("[Incorrect value]");
        else System.out.println("[Updated project status]");
    }

    @Override
    public void updateStatusByIndex() {
        System.out.println("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        if (projectService.findByIndex(index) == null) {
            System.out.println("[Project not found]");
            return;
        }
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        Status status = Status.valueOf(statusValue);
        final Project updatedStatusProject = projectService.updateStatusByIndex(index, status);
        if (updatedStatusProject == null) System.out.println("[Incorrect value]");
        else System.out.println("[Updated project status]");
    }

    @Override
    public void updateStatusByName() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        if (projectService.findByName(name) == null) {
            System.out.println("[Project not found]");
            return;
        }
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        Status status = Status.valueOf(statusValue);
        final Project updatedStatusProject = projectService.updateStatusByName(name, status);
        if (updatedStatusProject == null) System.out.println("[Incorrect value]");
        else System.out.println("[Updated project status]");
    }

    private void showProject(final Project project) {
        System.out.println("Id: " + project.getId() + "\n" +
                "Name: " + project.getName() + "\n" +
                "Description: " + project.getDescription() + "\n" +
                "Status: " + project.getStatus().getDisplayName());
    }

}