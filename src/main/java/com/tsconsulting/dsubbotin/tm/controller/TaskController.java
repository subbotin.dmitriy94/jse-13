package com.tsconsulting.dsubbotin.tm.controller;

import com.tsconsulting.dsubbotin.tm.api.controller.ITaskController;
import com.tsconsulting.dsubbotin.tm.api.service.ITaskService;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.model.Task;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("Enter name:");
        String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        String description = TerminalUtil.nextLine();
        taskService.create(name, description);
        System.out.println("[Create]");
    }

    @Override
    public void showTasks() {
        if (taskService.findAll().isEmpty()) System.out.println("[List tasks empty]");
        int index = 1;
        final List<Task> tasks = taskService.findAll();
        for (Task task : tasks) System.out.println(index++ + ". " + task);
    }

    @Override
    public void clearTasks() {
        taskService.clear();
        System.out.println("[Clear]");
    }

    @Override
    public void showById() {
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findById(id);
        if (task == null) {
            System.out.println("[Task not found]");
            return;
        }
        showTask(task);
    }

    @Override
    public void showByIndex() {
        System.out.println("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("[Task not found]");
            return;
        }
        showTask(task);
    }

    @Override
    public void showByName() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findByName(name);
        if (task == null) {
            System.out.println("[Task not found]");
            return;
        }
        showTask(task);
    }

    @Override
    public void removeById() {
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeById(id);
        if (task == null) System.out.println("[Task not found]");
        else System.out.println("[Task removed]");
    }

    @Override
    public void removeByIndex() {
        System.out.println("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.removeByIndex(index);
        if (task == null) System.out.println("[Task not found]");
        else System.out.println("[Task removed]");
    }

    @Override
    public void removeByName() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeByName(name);
        if (task == null) System.out.println("[Task not found]");
        else System.out.println("[Task removed]");
    }

    @Override
    public void updateById() {
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        if (taskService.findById(id) == null) {
            System.out.println("[Task not found]");
            return;
        }
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        Task updatedTask = taskService.updateById(id, name, description);
        if (updatedTask == null) System.out.println("[Incorrect value]");
        else System.out.println("[Task updated]");
    }

    @Override
    public void updateByIndex() {
        System.out.println("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        if (taskService.findByIndex(index) == null) {
            System.out.println("[Task not found]");
            return;
        }
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        Task updatedTask = taskService.updateByIndex(index, name, description);
        if (updatedTask == null) System.out.println("[Incorrect value]");
        else System.out.println("[Task updated]");
    }

    @Override
    public void startById() {
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.startById(id);
        if (task == null) System.out.println("[Task not found]");
    }

    @Override
    public void startByIndex() {
        System.out.println("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.startByIndex(index);
        if (task == null) System.out.println("[Task not found]");
    }

    @Override
    public void startByName() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.startByName(name);
        if (task == null) System.out.println("[Task not found]");
    }

    @Override
    public void finishById() {
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.finishById(id);
        if (task == null) System.out.println("[Task not found]");
    }

    @Override
    public void finishByIndex() {
        System.out.println("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.finishByIndex(index);
        if (task == null) System.out.println("[Task not found]");
    }

    @Override
    public void finishByName() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.finishByName(name);
        if (task == null) System.out.println("[Task not found]");
    }

    @Override
    public void updateStatusById() {
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        if (taskService.findById(id) == null) {
            System.out.println("[Task not found]");
            return;
        }
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        Status status = Status.valueOf(statusValue);
        final Task updatedStatusTask = taskService.updateStatusById(id, status);
        if (updatedStatusTask == null) System.out.println("[Incorrect value]");
        else System.out.println("[Updated task status]");
    }

    @Override
    public void updateStatusByIndex() {
        System.out.println("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        if (taskService.findByIndex(index) == null) {
            System.out.println("[Task not found]");
            return;
        }
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        Status status = Status.valueOf(statusValue);
        final Task updatedStatusTask = taskService.updateStatusByIndex(index, status);
        if (updatedStatusTask == null) System.out.println("[Incorrect value]");
        else System.out.println("[Updated task status]");
    }

    @Override
    public void updateStatusByName() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        if (taskService.findByName(name) == null) {
            System.out.println("[Task not found]");
            return;
        }
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        Status status = Status.valueOf(statusValue);
        final Task updatedStatusTask = taskService.updateStatusByName(name, status);
        if (updatedStatusTask == null) System.out.println("[Incorrect value]");
        else System.out.println("[Updated task status]");
    }

    private void showTask(final Task task) {
        System.out.println("Id: " + task.getId() + "\n" +
                "Name: " + task.getName() + "\n" +
                "Description: " + task.getDescription() + "\n" +
                "Status: " + task.getStatus().getDisplayName() + "\n" +
                "Project id: " + task.getProjectId()
        );
    }

}