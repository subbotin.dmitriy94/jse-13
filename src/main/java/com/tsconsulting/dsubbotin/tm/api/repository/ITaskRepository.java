package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(final Task task);

    void remove(final Task task);

    List<Task> findAll();

    void clear();

    boolean existById(final String id);

    Task findById(final String id);

    Task findByIndex(final int index);

    Task findByName(final String name);

    Task removeById(final String id);

    Task removeByIndex(final int index);

    Task removeByName(final String name);

    Task updateBuyId(final String id, final String name, final String description);

    Task updateBuyIndex(final int index, final String name, final String description);

    Task startById(final String id);

    Task startByIndex(final int index);

    Task startByName(final String name);

    Task finishById(final String id);

    Task finishByIndex(final int index);

    Task finishByName(final String name);

    Task updateStatusById(final String id, final Status status);

    Task updateStatusByIndex(final int index, final Status status);

    Task updateStatusByName(final String name, final Status status);

    Task bindTaskToProjectById(final String projectId, final String taskId);

    Task unbindTaskById(final String id);

    List<Task> findAllByProjectId(final String id);

    void removeAllTaskByProjectId(final String id);

}
