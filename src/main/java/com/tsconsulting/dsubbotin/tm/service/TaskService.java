package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.api.service.ITaskService;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) return;
        Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @Override
    public void add(final Task task) {
        if (task == null) return;
        taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findById(id);
    }

    @Override
    public Task findByIndex(final Integer index) {
        if (index < 0) return null;
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findByName(name);
    }

    @Override
    public Task removeById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(final int index) {
        if (index < 0) return null;
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task removeByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.removeByName(name);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        return taskRepository.updateBuyId(id, name, description);
    }

    @Override
    public Task updateByIndex(final int index, final String name, final String description) {
        if (index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        return taskRepository.updateBuyIndex(index, name, description);
    }

    @Override
    public Task startById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.startById(id);
    }

    @Override
    public Task startByIndex(final int index) {
        if (index < 0) return null;
        return taskRepository.startByIndex(index);
    }

    @Override
    public Task startByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.startByName(name);
    }

    @Override
    public Task finishById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.finishById(id);
    }

    @Override
    public Task finishByIndex(final int index) {
        if (index < 0) return null;
        return taskRepository.finishByIndex(index);
    }

    @Override
    public Task finishByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.finishByName(name);
    }

    @Override
    public Task updateStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.updateStatusById(id, status);
    }

    @Override
    public Task updateStatusByIndex(final int index, final Status status) {
        if (index < 0) return null;
        return taskRepository.updateStatusByIndex(index, status);
    }

    @Override
    public Task updateStatusByName(final String name, final Status status) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.updateStatusByName(name, status);
    }

}
