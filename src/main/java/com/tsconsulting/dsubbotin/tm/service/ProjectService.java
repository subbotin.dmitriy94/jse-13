package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IProjectService;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public void add(final Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findById(id);
    }

    @Override
    public Project findByIndex(final int index) {
        if (index < 0) return null;
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name);
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        return projectRepository.updateBuyId(id, name, description);
    }

    @Override
    public Project updateByIndex(final int index, final String name, final String description) {
        if (index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        return projectRepository.updateBuyIndex(index, name, description);
    }

    @Override
    public Project startById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.startById(id);
    }

    @Override
    public Project startByIndex(final int index) {
        if (index < 0) return null;
        return projectRepository.startByIndex(index);
    }

    @Override
    public Project startByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.startByName(name);
    }

    @Override
    public Project finishById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.finishById(id);
    }

    @Override
    public Project finishByIndex(final int index) {
        if (index < 0) return null;
        return projectRepository.finishByIndex(index);
    }

    @Override
    public Project finishByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.finishByName(name);
    }

    @Override
    public Project updateStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.updateStatusById(id, status);
    }

    @Override
    public Project updateStatusByIndex(final int index, final Status status) {
        if (index < 0) return null;
        return projectRepository.updateStatusByIndex(index, status);
    }

    @Override
    public Project updateStatusByName(final String name, final Status status) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.updateStatusByName(name, status);
    }

}
